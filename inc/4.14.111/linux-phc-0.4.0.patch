--- a/acpi-cpufreq.c	2019-04-08 08:36:28.105182921 +0200
+++ b/acpi-cpufreq.c	2019-04-09 21:29:48.328543109 +0200
@@ -25,6 +25,13 @@
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  */
 
+/*
+ * This file has been patched with Linux PHC: www.linux-phc.org
+ * Patch version: linux-phc-0.3.199
+ *
+ * Dedicated to Fabrice Bellamy - Author of the very first PHC patches
+ */
+
 #define pr_fmt(fmt) KBUILD_MODNAME ": " fmt
 
 #include <linux/kernel.h>
@@ -60,6 +67,16 @@
 };
 
 #define INTEL_MSR_RANGE		(0xffff)
+#define INTEL_MSR_VID_MASK	(0x00ff)	/* VID */
+#define INTEL_MSR_FID_MASK	(0x1f00)	/* Bit 12:8=FID */
+#define INTEL_MSR_SLFM_MASK	(0x8000)	/* Bit 15=SLFM / DFFS (Dynamic FSB Frequency Switching) */
+#define INTEL_MSR_NIF_MASK	(0x4000)	/* Bit 14=Non integer FIDS ("half FIDs") */
+#define INTEL_FULL_MASK		(INTEL_MSR_SLFM_MASK | INTEL_MSR_NIF_MASK | INTEL_MSR_FID_MASK | INTEL_MSR_VID_MASK)	/* combine all masks */
+/*#define MSR_IA32_ABS		(0x000000ce)*/	/* Absolute Minimum (SLFM) and Absolute Maximum (IDA) Values stored here. Not yet in msr-index.h. */
+#define INTEL_MSR_FSB_MASK	(0x07)		/* First three Bits are intex of a FSB */
+#define INTEL_MSR_FID_SHIFT	(0x8)		/* Shift 2nd Byte down to the first one to have an integer starting at 0 */
+#define PHC_VERSION_STRING	"0.3.199-2"
+
 #define AMD_MSR_RANGE		(0x7)
 
 #define MSR_K7_HWCR_CPB_DIS	(1ULL << 25)
@@ -71,6 +88,7 @@
 	cpumask_var_t freqdomain_cpus;
 	void (*cpu_freq_write)(struct acpi_pct_register *reg, u32 val);
 	u32 (*cpu_freq_read)(struct acpi_pct_register *reg);
+	acpi_integer *original_controls;
 };
 
 /* acpi_perf_data is a pointer to percpu data. */
@@ -188,6 +206,9 @@
 cpufreq_freq_attr_rw(cpb);
 #endif
 
+static unsigned int phc_unlock;
+static unsigned int phc_forceida;
+
 static int check_est_cpu(unsigned int cpuid)
 {
 	struct cpuinfo_x86 *cpu = &cpu_data(cpuid);
@@ -223,16 +244,22 @@
 	struct cpufreq_frequency_table *pos;
 	struct acpi_processor_performance *perf;
 
-	if (boot_cpu_data.x86_vendor == X86_VENDOR_AMD)
+	perf = to_perf_data(data);
+
+	if (boot_cpu_data.x86_vendor == X86_VENDOR_AMD) {
 		msr &= AMD_MSR_RANGE;
-	else
-		msr &= INTEL_MSR_RANGE;
 
-	perf = to_perf_data(data);
+		cpufreq_for_each_entry(pos, policy->freq_table)
+			if (msr == perf->states[pos->driver_data].status)
+				return pos->frequency;
+	} else {
+		u32 fid;
+		fid = msr & INTEL_MSR_FID_MASK;
 
-	cpufreq_for_each_entry(pos, policy->freq_table)
-		if (msr == perf->states[pos->driver_data].status)
-			return pos->frequency;
+		cpufreq_for_each_entry(pos, policy->freq_table)
+			if (fid == (perf->states[pos->driver_data].status & INTEL_MSR_FID_MASK))
+				return pos->frequency;
+	}
 	return policy->freq_table[0].frequency;
 }
 
@@ -839,6 +866,15 @@
 
 	policy->fast_switch_possible = !acpi_pstate_strict &&
 		!(policy_is_shared(policy) && policy->shared_type != CPUFREQ_SHARED_TYPE_ANY);
+	
+	/*
+	 * force activating the feature. It does not really activate it but
+	 * pretend to the system that it is active.
+	 */
+	if (phc_forceida == 1) {
+		printk("PHC: WARNING. By setting the IDA CPU capability bit you override CPUID feature data. This may result in unexpected problems.\n");
+		set_cpu_cap(c, X86_FEATURE_IDA);
+	}
 
 	return result;
 
@@ -865,6 +901,7 @@
 	policy->driver_data = NULL;
 	acpi_processor_unregister_performance(data->acpi_perf_cpu);
 	free_cpumask_var(data->freqdomain_cpus);
+	kfree(data->original_controls);
 	kfree(policy->freq_table);
 	kfree(data);
 
@@ -882,27 +919,6 @@
 	return 0;
 }
 
-static struct freq_attr *acpi_cpufreq_attr[] = {
-	&cpufreq_freq_attr_scaling_available_freqs,
-	&freqdomain_cpus,
-#ifdef CONFIG_X86_ACPI_CPUFREQ_CPB
-	&cpb,
-#endif
-	NULL,
-};
-
-static struct cpufreq_driver acpi_cpufreq_driver = {
-	.verify		= cpufreq_generic_frequency_table_verify,
-	.target_index	= acpi_cpufreq_target,
-	.fast_switch	= acpi_cpufreq_fast_switch,
-	.bios_limit	= acpi_processor_get_bios_limit,
-	.init		= acpi_cpufreq_cpu_init,
-	.exit		= acpi_cpufreq_cpu_exit,
-	.resume		= acpi_cpufreq_resume,
-	.name		= "acpi-cpufreq",
-	.attr		= acpi_cpufreq_attr,
-};
-
 static enum cpuhp_state acpi_cpufreq_online;
 
 static void __init acpi_cpufreq_boost_init(void)
@@ -936,6 +952,608 @@
 		cpuhp_remove_state_nocalls(acpi_cpufreq_online);
 }
 
+/* sysfs interface to change operating points voltages */
+
+/* Get Bits for FID */
+static unsigned int extract_fid_from_control(unsigned int control)
+{
+	return ((control & INTEL_MSR_FID_MASK) >> INTEL_MSR_FID_SHIFT);
+}
+
+/* Get Bit marking the SLFM / DFFS */
+static unsigned int extract_slfm_from_control(unsigned int control)
+{
+	/* return "1" if SLFM Bit is set */
+	return (((control & INTEL_MSR_SLFM_MASK) >> INTEL_MSR_FID_SHIFT) > 0);
+}
+
+/* Get Bit marking the Non Integer FIDs */
+static unsigned int extract_nif_from_control(unsigned int control)
+{
+	/* return "1" if NIF Bit is set */
+	return (((control & INTEL_MSR_NIF_MASK) >> INTEL_MSR_FID_SHIFT) > 0);
+}
+
+/* Get Bits for VID */
+static unsigned int extract_vid_from_control(unsigned int control)
+{
+	return (control & INTEL_MSR_VID_MASK);
+}
+
+/* check if the cpu we are running on is capable of setting new control data */
+static bool check_cpu_control_capability(struct cpufreq_policy *policy)
+{
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	if (unlikely(data == NULL ||
+	             policy->freq_table == NULL ||
+	             data->cpu_feature != SYSTEM_INTEL_MSR_CAPABLE)) {
+		return false;
+	} else {
+		return true;
+	};
+}
+
+static ssize_t check_origial_table (struct cpufreq_policy *policy)
+{
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	struct acpi_processor_performance *perf;
+	struct cpufreq_frequency_table *freq_table;
+	unsigned int state_index;
+
+	perf = to_perf_data(data);
+	freq_table = policy->freq_table;
+
+	/* Backup original control values */
+	if (data->original_controls == NULL) {
+		data->original_controls = kcalloc(perf->state_count,
+		                                  sizeof(acpi_integer), GFP_KERNEL);
+		if (data->original_controls == NULL) {
+			printk("failed to allocate memory for original control values\n");
+			return -ENOMEM;
+		}
+		for (state_index = 0; state_index < perf->state_count; state_index++) {
+			data->original_controls[state_index] = perf->states[state_index].control;
+		}
+	}
+	return 0;
+}
+
+struct new_oppoints {
+	unsigned long pstate; 		/* PSTATE value */
+	unsigned long statefreq;	/* frequency fot that PSTATE */
+};
+
+/*
+ * display phc's controls for the cpu
+ * (frequency id's and related* voltage id's)
+ */
+static ssize_t show_freq_attr_controls(struct cpufreq_policy *policy, char *buf) {
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	struct acpi_processor_performance *perf;
+	struct cpufreq_frequency_table *freq_table;
+	ssize_t count = 0;
+	unsigned int i;
+	
+	/* check if CPU is capable of changing controls */
+	if (!check_cpu_control_capability(policy)) return -ENODEV;
+
+	perf = to_perf_data(data);
+	freq_table = policy->freq_table;
+
+	for (i = 0; freq_table[i].frequency != CPUFREQ_TABLE_END; i++) {
+		count += sprintf(&buf[count], "%08x", (int)perf->states[freq_table[i].driver_data].control);
+		/* add seperating space */
+		if(freq_table[i+1].frequency != CPUFREQ_TABLE_END) count += sprintf(&buf[count], " ");
+	}
+	/* add line break */
+	count += sprintf(&buf[count], "\n");
+	return count;
+
+}
+
+/* display acpi's default controls for the CPU */
+static ssize_t show_freq_attr_default_controls(struct cpufreq_policy *policy, char *buf) {
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	struct cpufreq_frequency_table *freq_table;
+	unsigned int i;
+	ssize_t count = 0;
+	ssize_t retval;
+
+	/* check if CPU is capable of changing controls */
+	if (!check_cpu_control_capability(policy)) return -ENODEV;
+
+	retval = check_origial_table(policy);
+	if (0 != retval)
+		return retval;
+
+	freq_table = policy->freq_table;
+
+	for (i = 0; freq_table[i].frequency != CPUFREQ_TABLE_END; i++) {
+		count += sprintf(&buf[count], "%08x", (int)data->original_controls[freq_table[i].driver_data]);
+		/* add seperating space */
+		if(freq_table[i+1].frequency != CPUFREQ_TABLE_END) count += sprintf(&buf[count], " ");
+	}
+
+	/* add NewLine */
+	count += sprintf(&buf[count], "\n");
+	return count;
+}		
+
+/*
+ * An array of BUS frequencies with an integer as index we get the Index from an
+ * MSR register and can "translate" it to a MHz value of the BUS (FSB) so we can
+ * calculate the new System Frequency if the user sets new Op-Points
+ */
+static unsigned int get_fsb_freq(unsigned int cpu) {
+	static int fsb_table[7] = {
+	267,	/* msr index value of 0 */
+	133,	/* msr index value of 1 */
+	200,	/* msr index value of 2 */
+	167,	/* msr index value of 3 */
+	333,	/* msr index value of 4 */
+	100,	/* msr index value of 5 */
+	400,	/* msr index value of 6 */
+	};
+	u32 lofsb_index = 0;
+	u32 hifsb_index = 0;
+	unsigned int retval;
+
+	/* read BUS-Frequency index - could get tricky on i3/i5/i7 CPUs */
+	rdmsr_on_cpu(cpu, MSR_FSB_FREQ, &lofsb_index, &hifsb_index);
+	retval = (lofsb_index & INTEL_MSR_FSB_MASK);
+
+	/* is there an index we do not know yet? */
+	if (retval > (sizeof(fsb_table)/sizeof(int))) {
+		printk("PHC: WARNING. FSB Index (%u) is not known. Please report this.\n", retval);
+		retval = 0;
+	} else {
+		retval = fsb_table[retval];
+	}
+	return retval;
+}
+
+/*
+ * Store controls (pstate data) from sysfs file to the pstate table This is only
+ * allowed if this module is loaded with "phc_unlock" enabled.  Because we allow
+ * setting new pstates here we cannot check much to prevent user from setting
+ * dangerous values. The User hould handle this possibility with care and only
+ * if he is aware of what he is doing.
+ */
+static ssize_t store_freq_attr_controls(struct cpufreq_policy *policy, const char *buf, size_t count) {
+
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	struct acpi_processor_performance *perf;
+	struct cpufreq_frequency_table *freq_table;
+	struct new_oppoints *new_oppoint_table;	/* save a struct while sorting */
+	struct cpuinfo_x86 *c = &cpu_data(policy->cpu);
+
+	ssize_t	retval;			/* return value */
+	char *opstring;			/* one op-point (after seperating the buffer by delimeters) */
+	char *pbuf;			/* copy of buffer stored here */
+	unsigned int opcount = 0;	/* count of oppoints set through sysfs interface */
+	unsigned int i = 0;		/* used in a for loop */
+	unsigned int bus_freq = 0;	/* calculated Bus Frequency */
+	unsigned int curreqspeed = 0;	/* Speed equivalent value for current oppoint */
+	unsigned int lasteqspeed = 0;	/* Speed equivalent value for last oppoint */
+	unsigned int round;		/* variable used to buffer some values to round frequency */
+
+	/*-------------------- Do some Checks --------------------*/
+
+	/* CPU not compatible? */
+	if (!check_cpu_control_capability(policy)) return -ENODEV;
+
+	/* check original op-points table */
+	retval = check_origial_table(policy);
+	if (0 != retval)
+		return retval;
+
+	/* To calculate */
+	bus_freq = get_fsb_freq(policy->cpu);
+
+	/* can not continue without Bus-Freq */
+	if ((bus_freq == 0)){
+		printk("PHC: Unable to get FSB Bus frequency. Therefore we can not write own pstates. Please report this Error! \n");
+		return -ENODEV;
+	}		
+
+	perf = to_perf_data(data);
+	freq_table = policy->freq_table;
+	
+	/*-------------------- prepare --------------------*/
+
+	printk("PHC: Got new pstates through sysfs rawinterface.\n");
+	/* allocate enough memory for copy buffer */
+	pbuf = kmalloc(strlen(buf) ,GFP_KERNEL);
+	if (pbuf == NULL) {
+		printk("PHC: ERROR. Failed to allocate memory for buffer\n");
+		return -ENOMEM;
+	}
+	/* copy buffer "buf" to "pbuf" */
+	strcpy(pbuf,buf);
+
+	/*-------------------- parse SYSFS Interface File --------------------*/
+
+	/* counting the OP-Points (pstates) the user send through the sysfs Interface */
+
+	/*separate by SPACE */
+	while ((opstring = strsep((char**) &buf, " ")) != NULL) {
+		/* count possible OP-Points (Strings with length >= 3 chars) */
+		if (strlen(opstring) >= 3) {
+			opcount++;
+			printk("added: [%s]\n",opstring);
+		};
+	}
+
+	opcount--; /* FixMe! */
+	printk("PHC: Found 0 - %d OP-Points in SYSFS. Original table contains 0-%d OP-Points.\n", opcount, perf->state_count);
+
+	/*
+	 * currently we can not increase the count of pstates so we check if the
+	 * user is trying to set more
+	 */
+	if (opcount > perf->state_count){
+		printk("Adding additional OP-Points is not supported. You need to pass %d P-Points.\n",perf->state_count);
+		return -EINVAL;
+	}
+
+	if (opcount < perf->state_count){
+		printk("Removing OP-Points is not supported. You need to pass %d P-Points.\n",perf->state_count);
+		return -EINVAL;
+	}
+
+	/* allocate sufficient memory to store new pstate and additional data */
+	new_oppoint_table = kmalloc(opcount*sizeof(struct new_oppoints), GFP_KERNEL);
+
+	/* We are going to reuse that value */
+	opcount = 0;
+
+	/* Now we try to parse the sysfs file. The values shall be space-separated */
+	while (((opstring = strsep(&pbuf, " ")) != NULL)) {
+		/*
+		 * at least 3 chars are required to set FID and VID (see register
+		 * bitmap)
+		 */
+		if (strlen(opstring) >= 3) {
+
+			/* try to convert the string to an integer */
+			new_oppoint_table[opcount].pstate = strtoul(opstring, NULL, 16);
+
+			/*
+			 * we need to make sure that op-points are descending
+			 * therefore we calculate effective values to compare them later
+			 */
+			curreqspeed = extract_fid_from_control(new_oppoint_table[opcount].pstate);
+
+			/*
+			 * makes a bitshift rightwise. If SLFM = 1 it shifs by 1
+			 * (whats same like dividung by 2)
+			 */
+			curreqspeed = (curreqspeed >> extract_slfm_from_control(new_oppoint_table[opcount].pstate));
+
+			/*
+			 * by doubling the value we can add "1" instead of "0.5"
+			 * if NIF is set (no need to use float)
+			 */
+			curreqspeed *= 2;
+			if (extract_nif_from_control(new_oppoint_table[opcount].pstate)) curreqspeed = (curreqspeed+1);
+
+			/*
+			 * if this is not the first pstate we compare it with
+			 * the last one to keep sure pstates are in an
+			 * descending order
+			 */
+			if (opcount > 0) {
+				/* extract FID from current value */
+				lasteqspeed = extract_fid_from_control(new_oppoint_table[opcount-1].pstate);
+
+				/*
+				 * makes a bitshift rightwise. If SLFM = 1 it
+				 * shifs by 1 (whats same like dividung by 2)
+				 */
+				lasteqspeed = (lasteqspeed >> extract_slfm_from_control(new_oppoint_table[opcount-1].pstate));
+
+				/*
+				 * by doubling the value we can add "1" instead
+				 * of "0.5" o if NID is set (no need to use
+				 * float)
+				 */
+				lasteqspeed *= 2;	
+				if (extract_nif_from_control(new_oppoint_table[opcount].pstate)) lasteqspeed = (lasteqspeed+1);
+
+				if (curreqspeed > lasteqspeed) {
+					printk("PHC ERROR: The OPPoint at position %d (starting with 0) is higher than the previous. OPPoints need to be ascending.\n", opcount);
+					kfree(new_oppoint_table);
+					return -EINVAL;
+				};
+
+				if ((new_oppoint_table[opcount].pstate & INTEL_MSR_VID_MASK) > (new_oppoint_table[opcount-1].pstate & INTEL_MSR_VID_MASK)) {
+					printk("PHC ERROR: The VID for OPPoint at position %d (starting with 0) is higher than the previous. It is unlikely that a VID needs to be higher for a lower frequency.\n", opcount);
+					kfree(new_oppoint_table);
+					return -EINVAL;
+				}
+			};
+
+			printk("PHC: Calculated speed equivalent value for OP-Point %d is: %d\n", opcount, curreqspeed);
+
+			/* calculating the new frequency */
+			if (cpu_has(c, X86_FEATURE_IDA) & (opcount == 0)) {
+				/*
+				 * If this CPU offers IDA, the highest (first)
+				 * pstate is defining the data for IDA.  For
+				 * some reasons the calculated Frequency for IDA
+				 * is the same like for the default highest
+				 * pstate increased by 1MHz.  We will do the
+				 * same.
+				 */
+				new_oppoint_table[opcount].statefreq = ((bus_freq * (curreqspeed-1)) / 2);
+			} else {
+				new_oppoint_table[opcount].statefreq = ((bus_freq * curreqspeed) / 2);
+			}
+
+			/* round values */
+			round = new_oppoint_table[opcount].statefreq % 10;
+			if(round>5) new_oppoint_table[opcount].statefreq = new_oppoint_table[opcount].statefreq+(10 - round);
+			       else new_oppoint_table[opcount].statefreq = new_oppoint_table[opcount].statefreq - round;
+
+			/* add 1 MHz to frequency if this is an IDA pstate */
+			if (cpu_has(c, X86_FEATURE_IDA) & (opcount == 0)) { new_oppoint_table[opcount].statefreq++; };
+
+			/* calculate kHz from MHz */
+			new_oppoint_table[opcount].statefreq = new_oppoint_table[opcount].statefreq* 1000;
+			printk("PHC: New calculated frequency for pstate %d is: %lu\n", opcount, new_oppoint_table[opcount].statefreq);
+
+			opcount++;
+		} else {
+			if((*opstring == 32) | (*opstring == 10)) {
+				printk("ignoring extra SPACE or LineBreak\n");
+			} else {
+			   printk("PHC: ERROR. Malformed pstate data found at position %d. Stopping. Length: %d , content:[%s]\n", opcount, (int)strlen(opstring), opstring);
+			   return -EINVAL;
+			};
+		}
+	}
+
+	/*-------------------- copy new pstates to the acpi table --------------------*/
+	opcount--; /* fixme! */
+	printk("OPCount vor dem schreiben ist: %d\n", opcount);
+
+	for (i = 0; i <= opcount; i++) {
+		/* overwrite ACPI frequency table */
+		policy->freq_table[i].frequency = new_oppoint_table[i].statefreq;
+		/* overwrite Processor frequency table */
+		freq_table[i].frequency = 	new_oppoint_table[i].statefreq;
+		/* overwrite ACPI controls */
+		perf->states[i].control = 	new_oppoint_table[i].pstate;
+	}
+
+	for(i = 0; i < perf->state_count; i++) {
+		printk("perf @ %d: , %lu\n", i, (unsigned long)perf->states[i].control);
+	}
+	/*-------------------- resuming work --------------------*/	
+
+	retval = count;
+	data->resume = 1;
+	acpi_cpufreq_target(policy, perf->state);
+
+	return retval;
+}
+
+/* -------------------------- VIDS interface */
+
+/* display VIDs from current pstates */
+static ssize_t show_freq_attr_vids(struct cpufreq_policy *policy, char *buf) {
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	struct acpi_processor_performance *perf;
+	struct cpufreq_frequency_table *freq_table;
+	unsigned int i;
+	unsigned int vid;
+	ssize_t count = 0;
+	
+	/* check if CPU is capable of changing controls */
+	if (!check_cpu_control_capability(policy)) return -ENODEV;
+
+	perf = to_perf_data(data);
+	freq_table = policy->freq_table;
+
+	for (i = 0; freq_table[i].frequency != CPUFREQ_TABLE_END; i++) {
+		vid = extract_vid_from_control(perf->states[freq_table[i].driver_data].control);
+		count += sprintf(&buf[count], "%u", vid);
+
+		/* add seperating space */
+		if(freq_table[i+1].frequency != CPUFREQ_TABLE_END) count += sprintf(&buf[count], " ");
+	}
+	count += sprintf(&buf[count], "\n");
+
+	return count;
+}
+
+/* display VIDs from default pstates */
+static ssize_t show_freq_attr_default_vids(struct cpufreq_policy *policy, char *buf) {
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	struct cpufreq_frequency_table *freq_table;
+	unsigned int i;
+	unsigned int vid;
+	ssize_t count = 0;
+	ssize_t retval;
+	
+	/* check if CPU is capable of changing controls */
+	if (!check_cpu_control_capability(policy)) return -ENODEV;
+
+	retval = check_origial_table(policy);
+	if (0 != retval)
+		return retval;
+
+	freq_table = policy->freq_table;
+
+	for (i = 0; freq_table[i].frequency != CPUFREQ_TABLE_END; i++) {
+		vid = extract_vid_from_control(data->original_controls[freq_table[i].driver_data]);
+		count += sprintf(&buf[count], "%u", vid);
+		if(freq_table[i+1].frequency != CPUFREQ_TABLE_END) count += sprintf(&buf[count], " ");
+	}
+	count += sprintf(&buf[count], "\n");
+
+	return count;
+}
+
+/*
+ * Store VIDs for the related pstate.  We are going to do some sanity checks
+ * here to prevent users from setting higher voltages than the default one.
+ */
+static ssize_t store_freq_attr_vids(struct cpufreq_policy *policy, const char *buf, size_t count) {
+	struct acpi_cpufreq_data *data = policy->driver_data;
+	struct acpi_processor_performance *perf;
+	struct cpufreq_frequency_table *freq_table;
+	unsigned int freq_index;
+	unsigned int state_index;
+	unsigned int new_vid;
+	unsigned int original_vid;
+	unsigned int new_control;
+	unsigned int original_control;
+	const char *curr_buf = buf;
+	char *next_buf;
+	ssize_t retval;
+	
+	/* check if CPU is capable of changing controls */
+	if (!check_cpu_control_capability(policy)) return -ENODEV;
+
+	retval = check_origial_table(policy);
+	if (0 != retval)
+		return retval;
+
+	perf = to_perf_data(data);
+	freq_table = policy->freq_table;
+
+	printk("PHC: Got new VIDs through sysfs VID interface.\n");
+	/*
+	 * for each value taken from the sysfs interfalce (phc_vids) get entrys
+	 * and convert them to unsigned long integers
+	 */
+	for (freq_index = 0; freq_table[freq_index].frequency != CPUFREQ_TABLE_END; freq_index++) {
+		new_vid = simple_strtoul(curr_buf, &next_buf, 10);
+		if (next_buf == curr_buf) {
+			/* end of line? */
+			if ((curr_buf - buf == count - 1) && (*curr_buf == '\n')) {
+				curr_buf++;
+				break;
+			}
+			/*
+			 * if we didn't got end of line but there is nothing
+			 * more to read something went wrong...
+			 */
+			printk("PHC: Failed to parse vid value at %i (%s)\n", freq_index, curr_buf);
+			return -EINVAL;
+		}
+
+		state_index = freq_table[freq_index].driver_data;
+		original_control = data->original_controls[state_index];
+		original_vid = original_control & INTEL_MSR_VID_MASK;
+		
+		/*
+		 * before we store the values we do some checks to prevent users
+		 * to set up values higher than the default one
+		 */
+		if (new_vid <= original_vid) {
+			new_control = (original_control & ~INTEL_MSR_VID_MASK) | new_vid;
+			pr_debug("PHC: Setting control at %i to %x (default is %x)\n",
+			        freq_index, new_control, original_control);
+			perf->states[state_index].control = new_control;
+
+		} else {
+			printk("PHC: Skipping vid at %i, %u is greater than default %u\n",
+			       freq_index, new_vid, original_vid);
+		}
+
+		curr_buf = next_buf;
+		/*
+		 * jump over value seperators (space or comma).
+		 * There could be more than one space or comma character
+		 * to separate two values so we better do it using a loop.
+		 */
+		while ((curr_buf - buf < count) && ((*curr_buf == ' ') || (*curr_buf == ','))) {
+			curr_buf++;
+		}
+	}
+
+	/* set new voltage for current frequency */
+	data->resume = 1;
+	acpi_cpufreq_target(policy, perf->state);
+
+	return curr_buf - buf;
+}
+
+/* print out the phc version string set at the beginning of that file */
+static ssize_t show_freq_attr_phc_version(struct cpufreq_policy *policy, char *buf) {
+	ssize_t count = 0;
+	count += sprintf(&buf[count], "%s\n", PHC_VERSION_STRING);
+	return count;
+}
+
+/* display PHC version */
+static struct freq_attr cpufreq_freq_attr_phc_version =
+{
+	.attr = { .name = "phc_version", .mode = 0444 },
+	.show = show_freq_attr_phc_version,
+	.store = NULL,
+};
+
+/* display default rawcontrols */
+static struct freq_attr cpufreq_freq_attr_controls =
+{
+	.attr = { .name = "phc_rawcontrols", .mode = 0644 },
+	.show = show_freq_attr_controls,
+	.store = store_freq_attr_controls,
+};
+
+/* display default rawcontrols */
+static struct freq_attr cpufreq_freq_attr_default_controls =
+{
+	.attr = { .name = "phc_default_rawcontrols", .mode = 0444 },
+	.show = show_freq_attr_default_controls,
+	.store = NULL,
+};
+
+/* display current VIDs */
+static struct freq_attr cpufreq_freq_attr_vids =
+{
+	.attr = { .name = "phc_vids", .mode = 0644 },
+	.show = show_freq_attr_vids,
+	.store = store_freq_attr_vids,
+};
+
+/* display default VIDs */
+static struct freq_attr cpufreq_freq_attr_default_vids =
+{
+	.attr = { .name = "phc_default_vids", .mode = 0444 },
+	.show = show_freq_attr_default_vids,
+	.store = NULL,
+};
+
+static struct freq_attr *acpi_cpufreq_attr[] = {
+	&cpufreq_freq_attr_scaling_available_freqs,
+	&freqdomain_cpus,
+	&cpufreq_freq_attr_phc_version,
+	&cpufreq_freq_attr_controls,
+	&cpufreq_freq_attr_default_controls,
+	&cpufreq_freq_attr_vids,
+	&cpufreq_freq_attr_default_vids,
+#ifdef CONFIG_X86_ACPI_CPUFREQ_CPB
+	&cpb,
+#endif
+	NULL,
+};
+
+static struct cpufreq_driver acpi_cpufreq_driver = {
+	.verify		= cpufreq_generic_frequency_table_verify,
+	.target_index	= acpi_cpufreq_target,
+	.fast_switch	= acpi_cpufreq_fast_switch,
+	.bios_limit	= acpi_processor_get_bios_limit,
+	.init		= acpi_cpufreq_cpu_init,
+	.exit		= acpi_cpufreq_cpu_exit,
+	.resume		= acpi_cpufreq_resume,
+	.name		= "phc-intel",
+	.attr		= acpi_cpufreq_attr,
+};
+
 static int __init acpi_cpufreq_init(void)
 {
 	int ret;
@@ -994,6 +1612,15 @@
 }
 
 module_param(acpi_pstate_strict, uint, 0644);
+module_param(phc_unlock, uint, 0644);
+module_param(phc_forceida, uint, 0644);
+
+MODULE_PARM_DESC(phc_unlock,
+	"value 0 or non-zero. non-zero -> Enables altering OP-Points"
+	"(change FID, NIF, SLFM)");
+MODULE_PARM_DESC(phc_forceida,
+	"value 0 or non-zero. non-zero -> Forces setting X86_FEATURE_IDA to TRUE"
+	"(for buggy BIOSes that offer an IDA pstate but does not expose that feature in CPUID)");
 MODULE_PARM_DESC(acpi_pstate_strict,
 	"value 0 or non-zero. non-zero -> strict ACPI checks are "
 	"performed during frequency changes.");
