This package provides patch sets to build and install the
phc-intel kernel module, including

  * phc-intel-0.3.2 stable
  * phc-intel-0.4.0 testing

Version 0.3.2 is used by default. If you want to be a guinea
pig, switch to 0.4.0 with 'make brave' while 'make canny'
leads you back to stable. Those two targets tinker with the
Makefile itself, so you need to call them only when you
change your mind about which patch version to use. Invoking
those two targets with other targets on the same command
line does not do what you might think it should do.

Depending on your system, you have a bunch of choices for
installing the module and while you can tweak it here and
there, at the end it boils down to the following.

1. The generic approach

  make
  make install

2. Let others do the work

  make dkms_install

3. Don't cheat your package management with dkms < v2.8.8

  make dkms_mkdeb
   or
  make dkms_mkrpm

You should choose No. 3 whenever possible, because you will
get a source package which you can install directly into
your distribution package management system and DKMS will
magically do all the compile work for you. As a bonus you
can install the generated source package on other systems,
too.

For No. 1 to work, you need your kernel headers or kernel
source installed and also build utilities like make and gcc.
For No. 2 and 3, installing dkms and its dependencies should
do the trick. However, don't miss docs/ for further
information.

Supported Processors:
  Intel Pentium M
  Intel Core
  Intel Core 2

If you'd like to undervolt a Pentium M CPU with kernel 3.0
or newer and you are missing voltage controls, have a look
at http://www.linux-phc.org/forum/viewtopic.php?f=8&t=4984

Later processors like Core i/m/M are not supported by these
patches, however please drop a message if your processor
isn't listed here but works with these patches.

One last thing: I did not develop any of the patches but
only ported them to other kernel versions so the patches
will apply clean and the module can be build. It works for
me. My system works as expected. YMMV.
