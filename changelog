phc-intel-pack (rev51) Mon, 20 Jan 2025 22:02:14 +0100

  * Add kernel 6.13 support

phc-intel-pack (rev50) Mon, 18 Nov 2024 21:23:01 +0100

  * Add kernel 6.12 support

phc-intel-pack (rev49) Sun, 15 Sep 2024 20:45:31 +0200

  * Add kernel 6.11 support
  * Include changes for kernel
    - 6.6.41
    - 6.9.10

phc-intel-pack (rev48) Mon, 15 Jul 2024 21:27:04 +0200

  * Add kernel 6.10 support

phc-intel-pack (rev47) Mon, 30 Oct 2023 12:25:12 +0100

  * Add kernel 6.6 support

phc-intel-pack (rev46) Mon, 28 Aug 2023 09:05:38 +0200

  * Makefile:
    - Add more targets to SKIPKERNEL exemption
    - Disable recursive SKIPKERNEL shortcut. Not working anymore for
      newer dkms versions. And with more targets added to SKIPKERNEL
      exemption, the effect is less dramatic.
    - Fix dkms_uninstall for new versions of dkms and try to keep
      compatibility with older versions of `dkms status`
      + old format: module_name, version, kernel, arch: status
      + new format: module_name/version, kernel, arch: status
    - Fix dkms_install by separating add and install actions. They
      could be specified in the past on the same command line.
    - Add additional checks for shell commands
      Closes: https://gitlab.com/linux-phc/phc-intel/-/issues/4

phc-intel-pack (rev45) Tue, 27 Jun 2023 08:44:14 +0200

  * Add kernel 6.4 support
  * Makefile:
    - Include actual package version number in dkms.conf.
      Closes: https://gitlab.com/linux-phc/phc-intel/-/issues/3
    - Add brave and canny targets to SKIPKERNEL exemption.
      Closes: https://gitlab.com/linux-phc/phc-intel/-/issues/6
  * README:
    - Rename README.1st to README.txt and keep a symlink to the
      old name for compatibility.
      Closes: https://gitlab.com/linux-phc/phc-intel/-/issues/5
    - Mention that dkms removed support for dkms_mkdeb,
      dkms_mkrpm starting with version dkms v2.8.8.

phc-intel-pack (rev44) Mon, 20 Feb 2023 18:47:07 +0100

  * Add kernel 6.2 support

phc-intel-pack (rev43) Mon, 12 Dec 2022 10:35:22 +0100

  * Include changes for kernel 4.14.297
  * Include changes for kernel 4.19.266
  * Include changes for kernel 5.4.217

phc-intel-pack (rev42) Mon, 03 Oct 2022 10:31:14 +0200

  * Add kernel 6.0 support

phc-intel-pack (rev41) Mon, 10 Jan 2022 20:23:20 +0100

  * Makefile: Replace `which` with `command -v`
  * Add kernel 5.16 support

phc-intel-pack (rev40) Mon, 01 Nov 2021 08:13:14 +0100

  * Add kernel 5.15 support

phc-intel-pack (rev39) Mon, 28 Jun 2021 09:47:30 +0200

  * Add kernel 5.13 support
  * Include changes for kernel
    - 5.11.22
    - 5.12.5

phc-intel-pack (rev38) Mon, 26 Apr 2021 08:21:38 +0200

  * Add kernel 5.12 support
  * Include changes for kernel
    - 5.10.20
    - 5.11.3

phc-intel-pack (rev37) Wed, 17 Feb 2021 19:02:07 +0100

  * Add kernel 5.11 support
  * Include changes for kernel 5.10.17

phc-intel-pack (rev36) Mon, 14 Dec 2020 12:04:54 +0100

  * Add kernel 5.10 support
  * Include changes for kernel
    - 4.4.242
    - 4.9.424
    - 4.14.204
    - 4.19.155
    - 5.4.75
    - 5.9.5

phc-intel-pack (rev35) Mon, 12 Oct 2020 19:32:33 +0200

  * Add kernel 5.9 support

phc-intel-pack (rev34) Mon, 03 Aug 2020 08:45:22 +0200

  * Add kernel 5.8 support
  * Include changes for kernel
    - 3.16.85
    - 4.4.227

phc-intel-pack (rev33) Mon, 01 Jun 2020 08:44:31 +0200

  * Include changes for kernel 5.7

phc-intel-pack (rev32) Mon, 25 Nov 2019 12:15:10 +0100

  * dkms.conf: Fix deprecated SUBDIRS= usage for DKMS

phc-intel-pack (rev31) Mon, 08 Jul 2019 08:05:41 +0200

  * Include changes for kernel 5.2

phc-intel-pack (rev30) Mon, 06 May 2019 21:54:47 +0200

  * Makefile: Replace SUBDIRS= with M= as the former is
              deprecated and will be removed from kbuild.
  * Include changes for kernel
    - 4.14.111
    - 4.19.34
    - 5.0.7
    - 5.1

phc-intel-pack (rev29) Sat, 29 Dec 2018 16:41:51 +0100

  * Include changes for kernel 4.20
  * phc-intel-0.4.0 since kernel 3.16:
    - use fid instead of msr in extract_msr() comparison

phc-intel-pack (rev28) Mon, 13 Aug 2018 13:10:07 +0200

  * Include changes for kernel 4.18

phc-intel-pack (rev27) Mon, 04 Jun 2018 09:10:40 +0200

  * Include changes for kernel 4.17
  * Remark on supported processors in README.1st

phc-intel-pack (rev26) Tue, 03 Apr 2018 11:35:56 +0200

  * Include changes for kernel 4.16

phc-intel-pack (rev25) Fri, 23 Feb 2018 17:55:05 +0100

  * Include changes for kernel
    - 4.9.83
    - 4.14.21
    - 4.15.5

phc-intel-pack (rev24) Mon, 20 Feb 2017 07:29:57 +0100

  * Add 4.10 kernel support

phc-intel-pack (rev23) Mon, 03 Oct 2016 10:59:27 +0200

  * Add 4.8 kernel support

phc-intel-pack (rev22) Tue, 26 Jul 2016 12:37:20 +0200

  * Add 4.7 kernel support

phc-intel-pack (rev21.1) Wed, 22 Jun 2016 21:31:21 +0200

  * Disable parallel make so the build area is ready in time

phc-intel-pack (rev21) Mon, 16 May 2016 10:10:56 +0200

  * Add 4.6 kernel support

phc-intel-pack (rev20) Mon, 14 Mar 2016 09:12:28 +0100

  * Add 4.5 kernel support

phc-intel-pack (rev19) Mon, 02 Nov 2015 07:37:58 +0100

  * Kernel support:
    - Add 4.3
    - Include changes for 4.2.4
    - Other changes from kernel 4.3 onwards:
      + rename acpi_processor_performance *acpi_data to *perf
      + fix whitespaces
      + minor adjustments to and follow Linux style guide for comments
      + remove unused code from 0.4.0 patch

phc-intel-pack (rev18) Sun, 30 Aug 2015 23:12:28 +0200

  * Refresh patches for kernel 4.2
  * Remove deprecated code for kernel >= 3.13

phc-intel-pack (rev17) Mon, 30 Mar 2015 17:57:14 +0200

  * Makefile:
    - Another shot to reduce maintenance effort on simple kernel rename.
    - minor cosmetic changes
  * dkms.conf:
    - Remove BUILD_EXCLUSIVE_KERNEL. The makefile will error out
      anyway if something does not fit.

phc-intel-pack (rev16) Mon, 04 Aug 2014 18:45:30 +0200

  * Add 3.16 kernel support

phc-intel-pack (rev15.1) Tue, 10 Jun 2014 23:50:32 +0200

  * Missed one change for kernel 3.15. Should be working now.

phc-intel-pack (rev15) Mon, 09 Jun 2014 11:40:17 +0200

  * Add 3.15 kernel support
  * Change cpufreq_driver.name to phc-intel for kernel >=3.1

phc-intel-pack (rev14) Mon, 31 Mar 2014 10:19:13 +0200

  * Add 3.14 kernel support

phc-intel-pack (rev13.2) Mon, 10 Feb 2014 11:27:57 +0100

  * Change cpufreq_driver.name from acpi-cpufreq to phc-intel for kernel
    >= 3.7. In Ubuntu phc-intel can be activated on the kernel command
    line with cupfreq_driver=phc-intel if acpi-cpufreq is built into the
    kernel. It would fail to load with "invalid argument". This change
    should fix it.

phc-intel-pack (rev13.1) Tue, 28 Jan 2014 20:59:36 +0100

  * Fix 0.4.0 patch for kernel 3.13

phc-intel-pack (rev13) Mon, 20 Jan 2014 17:09:20 +0100

  * Add 3.13 kernel support

phc-intel-pack (rev12) Mon, 04 Nov 2013 08:44:32 +0100

  * Add 3.12 kernel support

phc-intel-pack (rev11) Tue, 03 Sep 2013 10:57:24 +0200

  * Add 3.11 kernel support
  * Rework patch selection method

phc-intel-pack (rev10.1) Tue, 23 Jul 2013 18:52:07 +0200

  * Wrong patch set was used for kernel 3.1 to 3.6

phc-intel-pack (rev10) Sun, 14 Jul 2013 14:28:45 +0200

  * Adjust for changes in kernel 3.9.6 and 3.10

phc-intel-pack (rev9.1) Sun, 05 May 2013 19:02:52 +0200

  * Fixed syntax error in Makefile

phc-intel-pack (rev9) Mon, 29 Apr 2013 11:41:54 +0200

  * Add changes for kernel 3.9

phc-intel-pack (rev8) Sun, 24 Feb 2013 21:59:09 +0100

  * Add 3.7.5 kernel support

phc-intel-pack (rev7) Tue, 11 Dec 2012 10:07:58 +0100

  * Added 3.7 kernel support

phc-intel-pack (rev6) Sat, 19 May 2012 20:34:50 +0200

  * Makefile:
    - It's not necessary anymore to upgrade the package if merely the
      kernel version but no code has changed.

phc-intel-pack (rev5) Mon, 19 Mar 2012 09:58:53 +0100

  * Kernel support:
    - Added 3.3 stable
    - Added links to 2.6.40-43 for compatibility with Fedora

phc-intel-pack (rev4) Thu, 05 Jan 2012 10:37:07 +0100

  * Kernel support:
    - Added 3.2 stable

phc-intel-pack (rev3) Mon, 24 Oct 2011 16:22:34 +0200

  * Makefile:
    - Allow make to continue without kernel headers on targets which
      don't need them (e.g.make clean).
  * Kernel support:
    - Added 3.1 stable

phc-intel-pack (rev2) Fri, 22 Jul 2011 10:32:47 +0200

  * Extend makefile and dkms.conf to support 3.x kernel
  * Kernel support:
    - Added 3.0

phc-intel-pack (rev1) Thu, 19 May 2011 12:59:58 +0200

  * Kernel support:
    - Added 2.6.39
    - Include changes in longterm
      + 2.6.33.8
      + 2.6.34.9

phc-intel-pack () Fri, 18 Mar 2011 16:50:39 +0100

  * Added phc-intel-0.4.0 taken from test-release-0.3.199-3.tar.bz2
  * Changes in Makefile
    - test release inclusion:
      + Added 'brave' and 'canny' targets to switch 0.3.2<->0.4.0
    - Bugfix
      + dkms_install failed because of logic error in dkms_mod_check
  * Kernel support:
    - Added 2.6.38 stable (no changes regarding -rc7)

phc-intel (0.3.2-13) Wed, 02 Mar 2011 18:00:00 +0100

  * Based on phc-intel-0.3.2-12-1.tar.bz2 and phc-intel-0.3.2-10.tar.bz2
  * Reworked Makefile:
    - Added DKMS support
    - No need for external patch script
  * Kernel support:
    - stable and longterm from 2.6.27 to 2.6.37
    - 2.6.38-rc7
